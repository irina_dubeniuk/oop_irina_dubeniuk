<html>
	<head>
		<title>Dubeniuk I.O.</title>
		<!--styles-->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/animate.css">
		<link rel="stylesheet" href="css/style.css">
		<!--JSes-->
		<script src="js/jquery-2.1.1.js"></script>
		<script src="js/plotly-latest.min.js"></script>
		
	</head>
	<body>
			<div class="row border-bottom white-bg page-heading">
                <div class="col-xs-10">
                    <h2>Object-Oriented Programming</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>The way we live</a>
                        </li>
                    </ol>
                </div>
                <div class="col-xs-2">
                </br>
                Irina Dubeniuk
                </div>
            </div>
            </br>
            </br>
            </br>
	<div class="row">

	<div class="col-xs-11">
	<div class="ibox col-md-2 col-md-offset-1 ">
	</br>
	</br>
	</br>
	<div class="ibox-title">
		<h3>We need statement to create graph!</h3>
	</div>
	<div class="ibox-content">
		<form method="Post" class="group-control">
		<div class="form-group">
			<input type="text" name="stat" class="form-control">
		</div>
			<input type="submit" value="Build" class="btn btn-primary form-control" onclick="Wait">
		</form>
	</div>


	</div>
		<div id="graph" class="col-md-9 "style="height: 350px">
					
				</div>
		<?php include 'calc.php'; ?>
		<?php if (isset($js)):?>
		<script type="text/javascript">
		var trace = <?= $js ?>;
		Plotly.newPlot('graph', [trace]);</script>
		<?php
			endif;
		?>
	</div>
	</div>
	</body>
	<script>

	</script>
</html>